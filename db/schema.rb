# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191210040129) do

  create_table "calls", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "activity"
    t.string   "name"
    t.string   "phone"
    t.string   "value"
    t.string   "value_displacement"
    t.integer  "deadline"
    t.integer  "cep"
    t.string   "street"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "number"
    t.date     "start_date"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "cpf_cnpj"
    t.boolean  "account_mobile"
    t.string   "street"
    t.string   "number"
    t.string   "city"
    t.string   "state"
    t.string   "cep"
    t.string   "neighborhood"
    t.string   "complement"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "demands", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "customer_id"
    t.integer  "employee_id"
    t.decimal  "value",       precision: 10
    t.string   "attachments"
    t.string   "description"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "status"
    t.string   "category"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["customer_id"], name: "index_demands_on_customer_id", using: :btree
    t.index ["employee_id"], name: "index_demands_on_employee_id", using: :btree
  end

  create_table "employees", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "cpf"
    t.date     "start_date"
    t.date     "date_birth"
    t.string   "function"
    t.string   "street"
    t.string   "number"
    t.string   "city"
    t.string   "cep"
    t.string   "state"
    t.string   "neighborhood"
    t.string   "complement"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "description"
    t.string   "category"
    t.boolean  "post"
    t.string   "main_image"
    t.string   "photos"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "providers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "cnpj"
    t.string   "service"
    t.string   "street"
    t.string   "number"
    t.string   "city"
    t.string   "cep"
    t.string   "state"
    t.string   "neighborhood"
    t.string   "complement"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.string   "confirm_password"
    t.date     "date_birth"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

end
