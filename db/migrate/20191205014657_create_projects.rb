class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :description
      t.string :category
      t.boolean :post
      t.string :main_image
      t.string :photos

      t.timestamps
    end
  end
end
