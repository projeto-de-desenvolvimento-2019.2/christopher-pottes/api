class CreateProviders < ActiveRecord::Migration[5.0]
  def change
    create_table :providers do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :cnpj
      t.string :service
      t.string :street
      t.string :number
      t.string :city
      t.string :cep
      t.string :state
      t.string :number
      t.string :neighborhood
      t.string :complement

      t.timestamps
    end
  end
end
