# Software de Gestão para Escritório de Arquitetura

## Pré Requisitos

#### API
* Ruby version ≥ 2.3
* Rails version ≥ 5
* MySQL

Para instalar o Ruby, segue o [tutorial](http://fbarrioscl.github.io/ruby/2016/04/21/rvm-5-configuracion.html)

Para instalar Ruby, Rails, Redis e MySQL segue o [tutorial](https://onebitcode.com/rails-linux/)

#### APP
* Expo

Para instalar o Expo, segue o [tutorial](https://docs.expo.io/versions/latest/get-started/installation/)

#### WEB
* VueJS
* NuxtJS

Para instalar o Vue, segue o [tutorial](https://br.vuejs.org/v2/guide/installation.html)

Para instalar o Vue Cli, segue o [tutorial](https://cli.vuejs.org/guide/installation.html)

Para instalar o Nuxt, segue o [tutorial](https://nuxtjs.org/guide/installation)

## Clonar projeto
Web - ssh: git@gitlab.com:projeto-de-desenvolvimento-2019.2/christopher-pottes/web.git
1. Após clonar o repositório web, abrir a pasta raiz no terminal
2. Executar o comando: `npm build` para instalar as dependencias local
3. Para levantar a aplicação, execute o comando: `npm run dev`

App - ssh: git@gitlab.com:projeto-de-desenvolvimento-2019.2/christopher-pottes/app.git
1. Após clonar o repositório app, abrir a pasta raiz no terminal
2. Executar o comando: `npm install` para instalar as dependências do projeto
3. Executar o comando: `expo start`

Api - ssh: git@gitlab.com:projeto-de-desenvolvimento-2019.2/christopher-pottes/api.git
1. Após clonar o repositório api, abrir a pasta raiz no terminal
2. Executar o comando: `bundle install` para instalar as gems e dependencias
3. Executar o comando: `rake db:create db:migrate db:seed` para criar e migrar as tabelas e popular o banco de dados
4. Executar o comando: `rails s` para levantar a aplicação

## Para utilizar o projeto em produção
Página pública - https://protected-mesa-14183.herokuapp.com/external

Dashboard interna - https://protected-mesa-14183.herokuapp.com/


## Documentação do projeto
Toda documentação do projeto encontra-se na wiki [clique aqui](https://gitlab.com/projeto-de-desenvolvimento-2019.2/christopher-pottes/api/wikis/home)

## Contribuição
1. Escolha o projeto e faça um fork (https://gitlab.com/projeto-de-desenvolvimento-2019.2/christopher-pottes/api/-/forks/new), 
(https://gitlab.com/projeto-de-desenvolvimento-2019.2/christopher-pottes/web/-/forks/new) ou (https://gitlab.com/projeto-de-desenvolvimento-2019.2/christopher-pottes/app/-/forks/new)
2. Crie uma nova branch com o nome da feature (git checkout -b feature/fooBar)
3. Confirme suas alterações (git commit -am 'Add some fooBar')
4. Empurre as modicações para o branch (git push origin feature/fooBar)
5. Crie um novo Pull Request

## Autoria
Christopher Pottes Carnal | christopherpottesc@gmail.com

## Licença
A aplicação conta com a seguinte licença de uso: [MIT](https://opensource.org/licenses/MIT)

