class Employee < ApplicationRecord

  has_many :demands, :through => :demands

  validates :name, presence: true
  validates :phone, presence: true
  validates :email, presence: true
  validates :cpf, presence: true
  validates :start_date, presence: true
  validates :date_birth, presence: true
  validates :street, presence: true
  validates :cep, presence: true
  validates :number, presence: true
  validates :city, presence: true
  validates :neighborhood, presence: true
  validates :state, presence: true
end
