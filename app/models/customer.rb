class Customer < ApplicationRecord

  has_many :demands, :through => :demands

  validates :name, presence: true
  validates :phone, presence: true
  validates :email, presence: true
  validates :cpf_cnpj, presence: true
  validates :account_mobile, presence: true
  validates :street, presence: true
  validates :cep, presence: true
  validates :number, presence: true
  validates :city, presence: true
  validates :neighborhood, presence: true
  validates :state, presence: true
end
