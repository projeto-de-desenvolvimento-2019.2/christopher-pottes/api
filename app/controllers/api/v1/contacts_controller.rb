module Api
	module V1
class ContactsController < ApplicationController
  # before_action :set_contact, only: [:show, :update, :destroy]

  # GET /contacts
  def index
    contacts = Contact.order('created_at DESC');
    render json: { data:contacts }
  end
  # Listar cliente passando ID
  def show
    contact = Contact.find(params[:id])
    render json: {status: 'SUCCESS', message:'Loaded contact', data:contact},status: :ok
  end
  # Criar um novo cliente
  def create
    contact = Contact.new(contact_params)
    if contact.save
      render json: contact
    else
      render json: {status: 'ERROR', message:'ContactS not saved', data:contact.errors},status: :unprocessable_entity
    end
  end
  # Excluir cliente
  def destroy
    contact = Contact.find(params[:id])
    contact.destroy
    render json: {status: 'SUCCESS', message:'Deleted contact', data:contact},status: :ok
  end
  # Atualizar um cliente
  def update
    contact = Contact.find(params[:id])
    if contact.update_attributes(contact_params)
      render json: {status: 'SUCCESS', message:'Updated contact', data:contact},status: :ok
    else
      render json: {status: 'ERROR', message:'Contacts not update', data:contact.erros},status: :unprocessable_entity
    end
  end
  # Parametros aceitos
  private
  def contact_params
    params.permit(:name, :phone, :email, :message)
  end
end
end
end
