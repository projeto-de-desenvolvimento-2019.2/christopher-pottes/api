module Api
	module V1
		class ProvidersController < ApplicationController
			# Listar todos os fornecedores
			def index
				providers = Provider.order('created_at DESC');
				render json: {status: 'SUCCESS', message:'Demands carregados', data:providers},status: :ok
			end
			# Listar fornecedor passando ID
			def show
				provider = Provider.find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded demand', data:provider},status: :ok
			end
			# Criar um novo fornecedor
			def create
				provider = Provider.new(provider_params)
				if provider.save
					render json: {status: 'SUCCESS', message:'Saved demand', data:provider},status: :ok
				else
					render json: {status: 'ERROR', message:'Demands not saved', data:provider.erros},status: :unprocessable_entity
				end
			end
			# Excluir fornecedor
			def destroy
				provider = Provider.find(params[:id])
				provider.destroy
				render json: {status: 'SUCCESS', message:'Deleted demand', data:provider},status: :ok
			end
			# Atualizar um fornecedor
			def update
				provider = Provider.find(params[:id])
				if provider.update_attributes(provider_params)
					render json: {status: 'SUCCESS', message:'Updated demand', data:provider},status: :ok
				else
					render json: {status: 'ERROR', message:'Demands not update', data:provider.erros},status: :unprocessable_entity
				end
			end
			# Parametros aceitos
			private
			def provider_params
        params.permit(:name, :phone, :email, :cnpj, :service, :street, :cep, :number, :city, :state, :number, :neighborhood, :complement)
      end
		end
	end
end