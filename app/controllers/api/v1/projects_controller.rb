module Api
	module V1
		class ProjectsController < ApplicationController
			# Listar todos os fornecedores
			def index
				projects = Project.order('created_at DESC');
				render json: {status: 'SUCCESS', message:'Demands carregados', data:projects},status: :ok
			end
			# Listar fornecedor passando ID
			def show
				project = Project.find(params[:id])
				render json: {status: 'SUCCESS', message:'Loaded demand', data:project},status: :ok
			end
			# Criar um novo fornecedor
			def create
				project = Project.new(project_params)
				if project.save
					render json: {status: 'SUCCESS', message:'Saved demand', data:project},status: :ok
				else
					render json: {status: 'ERROR', message:'Demands not saved', data:project.erros},status: :unprocessable_entity
				end
			end
			# Excluir fornecedor
			def destroy
				project = Project.find(params[:id])
				project.destroy
				render json: {status: 'SUCCESS', message:'Deleted demand', data:project},status: :ok
			end
			# Atualizar um fornecedor
			def update
				project = Project.find(params[:id])
				if project.update_attributes(project_params)
					render json: {status: 'SUCCESS', message:'Updated demand', data:project},status: :ok
				else
					render json: {status: 'ERROR', message:'Demands not update', data:project.erros},status: :unprocessable_entity
				end
			end
			# Parametros aceitos
			private
			def project_params
        params.permit(:description, :category, :post, :main_image, :photos)
      end
		end
	end
end