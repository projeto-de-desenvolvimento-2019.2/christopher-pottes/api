module Api
	module V1
		class DashboardController < ApplicationController
			def index

				# Total de cada módulo
				# Clientes
				customers = Customer.all.length
				# Demandas
				demands = Demand.all.length;
				# Funcionários
				employees = Employee.all.length;
				# Projetos
				projects = Project.all.length;
				# Fornecedores
				providers = Provider.all.length;
				# Contatos
				contacts = Project.all.length;
				# os's
				calls = Call.all.length;
				

				# Objeto com os valores
				response = {
					customers: customers,
					demands: demands,
					employees: employees,
					calls: calls,
					projects: projects,
					providers: providers,
					contacts: contacts,
				}

				render json: {status: 'SUCCESS', message:'Informações carregadas', data: response}, status: :ok
			end
		end
	end
end