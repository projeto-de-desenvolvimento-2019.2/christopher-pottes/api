module Api
	module V1
    class UsersController < ApplicationController
  # before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    users = User.order('created_at DESC');
    render json: { data:users }
  end

  # GET /users/1
  def show
    user = User.find(params[:id])
		render json: {status: 'SUCCESS', message:'Loaded user', data:user},status: :ok
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    user = User.find(params[:id])
		if user.update_attributes(user_params)
			render json: {status: 'SUCCESS', message:'Updated user', data:user},status: :ok
		else
			render json: {status: 'ERROR', message:'users not update', data:user.erros},status: :unprocessable_entity
    end
    
  end

  # DELETE /users/1
  def destroy
    user = User.find(params[:id])
		user.destroy
		render json: {status: 'SUCCESS', message:'Deleted user', data:user},status: :ok
  end

    private
    # Use callbacks to share common setup or constraints between actions.
    # def set_user
    #   @user = User.find(params[:id])
    # end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.permit(:name, :email, :password, :confirm_password, :date_birth)
    end
    end
  end
end
